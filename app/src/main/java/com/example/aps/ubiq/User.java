package com.example.aps.ubiq;
import java.util.Date;

/**
 * Created by 0aps on 9/25/16.
 */
public class User {

    private String name;
    private String lastname;
    private String address;
    private Date birthdate;
    private String career;

    public User() {
    }

    public User(String name, String lastname, String address, Date birthdate, String career) {
        this.name = name;
        this.lastname = lastname;
        this.address = address;
        this.birthdate = birthdate;
        this.career = career;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getCareer() {
        return career;
    }

    public void setCareer(String career) {
        this.career = career;
    }

}
