package com.example.aps.ubiq;

/**
 * Created by 0aps on 10/11/16.
 */

public class Quarter {
    String id;
    String name;

    public Quarter() {
    }

    public Quarter(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return this.name;
    }

}


