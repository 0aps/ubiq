package com.example.aps.ubiq;

import java.io.Serializable;

/**
 * Created by 0aps on 10/10/16.
 */

@SuppressWarnings("serial") //With this annotation we are going to hide compiler warnings
public class History implements Serializable{
    private String name;
    private String block;
    private String user;

    public History() {
    }

    public History(String name, String block, String user) {
        this.name = name;
        this.block = block;
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
