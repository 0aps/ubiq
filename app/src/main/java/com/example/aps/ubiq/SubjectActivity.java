package com.example.aps.ubiq;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;


import java.util.ArrayList;


public class SubjectActivity extends AppCompatActivity {

    private History history;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject);
    }

    @Override
    protected void onStart() {
        super.onStart();
        History history = (History) getIntent().getSerializableExtra("history");
        this.history = history;
        loadSubjects();

        //TextView textView= (TextView)findViewById(R.id.history_name);
        //textView.setText(history.getName());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_subject, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_history) {
            Intent intent = new Intent(SubjectActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }
        else if (id == R.id.action_profile) {
            Intent intent = new Intent(SubjectActivity.this, ProfileActivity.class);
            startActivity(intent);
            finish();
        }
        else if (id == R.id.action_blocks) {
            Intent intent = new Intent(SubjectActivity.this, BlockActivity.class);
            startActivity(intent);
            finish();
        }



        return super.onOptionsItemSelected(item);
    }

    private void loadSubjects(){
        FirebaseDatabase database = FirebaseDatabase.getInstance();


        DatabaseReference subjectReference = database.getReference("subjects");
        Query query = subjectReference.orderByChild("block").equalTo(this.history.getBlock());

        ListView messagesView = (ListView) findViewById(R.id.subjectListView);
        final ArrayList<String> listItems = new ArrayList<>();

        final ArrayAdapter myAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listItems);
        messagesView.setAdapter(myAdapter);

        ChildEventListener childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                Subject subject = dataSnapshot.getValue(Subject.class);

                listItems.add(subject.getName());
                myAdapter.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(), "Added new subject.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
                //Log.d(TAG, "onChildChanged:" + dataSnapshot.getKey());
                // ...
                Toast.makeText(getApplicationContext(), "Updated subject.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                //Log.d(TAG, "onChildRemoved:" + dataSnapshot.getKey());
                // ...
                Toast.makeText(getApplicationContext(), "Deleted subject.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
                //Log.d(TAG, "onChildMoved:" + dataSnapshot.getKey());
                // ...
                //Toast.makeText(getApplicationContext(), "Moved  new block.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Log.w(TAG, "postComments:onCancelled", databaseError.toException());
                Toast.makeText(getApplicationContext(), "Failed to load subjects.", Toast.LENGTH_SHORT).show();
            }
        };

        query.addChildEventListener(childEventListener);
    }

}
