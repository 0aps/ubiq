package com.example.aps.ubiq;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


public class BlockActivity extends AppCompatActivity {

    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_block);
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadQuarterInformation();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_block, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_history) {
            Intent intent = new Intent(BlockActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }
        else if (id == R.id.action_profile) {
            Intent intent = new Intent(BlockActivity.this, ProfileActivity.class);
            startActivity(intent);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void loadQuarterInformation(){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference quartersReference = database.getReference("quarters");

        quartersReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Is better to use a List, because you don't know the size
                // of the iterator returned by dataSnapshot.getChildren() to
                // initialize the array
                Toast.makeText(getApplicationContext(), "Loading quarters ...", Toast.LENGTH_SHORT).show();
                final List<Quarter> quarters = new ArrayList<>();

                for (DataSnapshot quarterSnapShot : dataSnapshot.getChildren()) {
                    Quarter quarter = quarterSnapShot.getValue(Quarter.class);
                    quarter.setId(quarterSnapShot.getKey());
                    quarters.add(quarter);
                }

                Spinner quarterSpinner = (Spinner) findViewById(R.id.blocks_spinner);
                ArrayAdapter<Quarter> quartersAdapter = new ArrayAdapter<>(BlockActivity.this, android.R.layout.simple_spinner_item, quarters);

                quartersAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                quarterSpinner.setAdapter(quartersAdapter);

                quarterSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                        Quarter quarter = (Quarter) parentView.getItemAtPosition(position);
                        loadBlocksInformation(quarter);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parentView) {
                        // to do
                    }

                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void loadBlocksInformation(Quarter quarter){
        expListView = (ExpandableListView) findViewById(R.id.blocks_listview);
        prepareListData(quarter.getId());

        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);
        expListView.setAdapter(listAdapter);
    }


    /*
     * Preparing the list data
     */
    private void prepareListData(String id) {

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference blocksReference = database.getReference("blocks");
        Query query = blocksReference.orderByChild("quarter").equalTo(id);

        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Toast.makeText(getApplicationContext(), "Loading blocks ...", Toast.LENGTH_SHORT).show();

                for (DataSnapshot blockSnapshot: dataSnapshot.getChildren()) {
                    Block block = blockSnapshot.getValue(Block.class);
                    block.setId(blockSnapshot.getKey());

                    loadSubjectInformation(block);
                    listDataHeader.add(block.getName());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void loadSubjectInformation(final Block block){

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference subjectsReference = database.getReference("subjects");
        final String blockName = block.getName();
        Query query = subjectsReference.orderByChild("block").equalTo(block.getId());

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Toast.makeText(getApplicationContext(), "Loading subject for block "+ blockName +"...", Toast.LENGTH_SHORT).show();

                ArrayList<String> subjects = new ArrayList<>();
                for (DataSnapshot subjectSnapshot : dataSnapshot.getChildren()) {
                    Subject subject = subjectSnapshot.getValue(Subject.class);
                    subjects.add(subject.getName());
                }
                listDataChild.put(blockName, subjects);
                listAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

}
