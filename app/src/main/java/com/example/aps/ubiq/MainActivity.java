package com.example.aps.ubiq;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

//import com.firebase.ui.database.FirebaseListAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.Query;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Date;


public class MainActivity extends AppCompatActivity {

    private FirebaseDatabase database;
    private FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();
        this.database = FirebaseDatabase.getInstance();
        this.user = FirebaseAuth.getInstance().getCurrentUser();

        loadHistoryInformation();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_profile) {
            Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
            startActivity(intent);
            finish();
        }
        else if (id == R.id.action_blocks) {
            Intent intent = new Intent(MainActivity.this, BlockActivity.class);
            startActivity(intent);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void loadHistoryInformation(){
        DatabaseReference historyReference = this.database.getReference("history");
        Query query = historyReference.orderByChild("user").equalTo(this.user.getUid());


        ListView messagesView = (ListView) findViewById(R.id.blocksListView);
        final ArrayList<History> listItems = new ArrayList<>();

        final CustomAdapter myAdapter = new CustomAdapter(this, R.layout.my_items_layout, listItems);
        messagesView.setAdapter(myAdapter);

        messagesView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                History history = listItems.get(position);
                Toast.makeText(getApplicationContext(), "History selected: "+history.getName(), Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(MainActivity.this, SubjectActivity.class);
                intent.putExtra("history", history);
                startActivity(intent);
                finish();

            }
        });

        ChildEventListener childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                History history = dataSnapshot.getValue(History.class);

                listItems.add(history);
                myAdapter.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(), "Added new block.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
                //Log.d(TAG, "onChildChanged:" + dataSnapshot.getKey());
                // ...
                Toast.makeText(getApplicationContext(), "Updated block.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                //Log.d(TAG, "onChildRemoved:" + dataSnapshot.getKey());
                // ...
                Toast.makeText(getApplicationContext(), "Deletedblock.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
                //Log.d(TAG, "onChildMoved:" + dataSnapshot.getKey());
                // ...
                //Toast.makeText(getApplicationContext(), "Moved  new block.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Log.w(TAG, "postComments:onCancelled", databaseError.toException());
                Toast.makeText(getApplicationContext(), "Failed to load comments.", Toast.LENGTH_SHORT).show();
            }
        };

        query.addChildEventListener(childEventListener);
    }

    private void createDefaultUser(){
        DatabaseReference usersReference = this.database.getReference("users");

        User myUser = new User("Angel", "Santana", "alma rosa II, Calle Club de Leones, #18", new Date(), "Ingeniería en Software");

        DatabaseReference newUserReference = usersReference.child(this.user.getUid());
        newUserReference.setValue(myUser);
    }

}

