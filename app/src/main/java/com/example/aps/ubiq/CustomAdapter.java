package com.example.aps.ubiq;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 0aps on 9/28/16.
 */
public class CustomAdapter extends ArrayAdapter<History> {

    public CustomAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public CustomAdapter(Context context, int resource, ArrayList<History> items) {
        super(context, resource, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.my_items_layout, null);
        }

        History history =  getItem(position);
        String name = history.getName();

        if(name != null){
            Button myButton = (Button) v.findViewById(R.id.list_view_button);
            myButton.setText(name);
        }

        return v;
    }

}
