package com.example.aps.ubiq;

/**
 * Created by 0aps on 10/10/16.
 */
public class Subject {
    private String block;
    private String name;

    public Subject() {
    }

    public Subject(String block, String name) {
        this.block = block;
        this.name = name;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
