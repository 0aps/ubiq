package com.example.aps.ubiq;

/**
 * Created by 0aps on 9/27/16.
 */
public class Block {

    private String id;
    private String name;
    private String quarter;

    public Block() {
    }

    public Block(String id, String name, String quarter) {
        this.id = id;
        this.name = name;
        this.quarter = quarter;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuarter() {
        return quarter;
    }

    public void setQuarter(String quarter) {
        this.quarter = quarter;
    }
}
